from django.conf.urls import url, include
from django.contrib import admin
from .models import *
from .views import *
from .models import *

urlpatterns = [
    # pacientes
    url(r'^paciente/listar$', PacienteList.as_view(), name='paciente_list'),
    url(r'^paciente/crear$', PacienteCreate.as_view(), name='paciente_crear'),
    url(r'^paciente/editar$', PacienteUpdate.as_view(), name='paciente_editar'),
    url(r'^paciente/eliminar$', PacienteDelete.as_view(), name='paciente_eliminar'),
]