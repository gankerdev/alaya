from django.db import models
from fichaClinica.models import FichaClinica

# Create your models here.
class Pacientes(models.Model):
	nombre = models.CharField(max_length=255, null=False)
	rut = models.CharField(max_length=10, null=False)
	fecha_nacimiento = models.DateTimeField(auto_now_add=False, auto_now=False)
	fecha_ingreso = models.DateTimeField(auto_now_add=False, auto_now=False)
	ficha_clinica = models.ForeignKey(FichaClinica, null=True, blank=True, on_delete=models.CASCADE)

def __str__(self):
    return self.nombre