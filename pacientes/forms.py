from django import forms

from .models import Pacientes


class PacientesForm(forms.ModelForm):
    class Meta:
        model = Pacientes

        fields = [
            'nombre',
            'rut',
            'fecha_nacimiento',
            'fecha_ingreso',
            'ficha_clinica',
        ]

        labels = {
            'nombre': 'Nombre',
            'rut': 'Rut',
            'fecha_nacimiento': 'Fecha de nacimiento',
            'fecha_ingreso': 'Fecha de ingreso',
            'ficha_clinica': 'Ficha clinica',
        }

        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Ingrese nombre paciente...'}),
            'rut': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Ingrese rut paciente...'}),
            'fecha_nacimiento': forms.SelectDateWidget(),
            'fecha_ingreso': forms.SelectDateWidget(),
            'ficha_clinica': forms.Select(attrs={'class': 'form-control'}),
        }

