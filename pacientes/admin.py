from django.contrib import admin
from .models import Pacientes

# Register your models here.
class AdminPaciente(admin.ModelAdmin):
	list_display = ["nombre","rut", "fecha_nacimiento", "fecha_ingreso", "ficha_clinica"] 
	list_filter = ["nombre",  "rut"]
	list_search = ["nombre", "rut"]

admin.site.register(Pacientes)