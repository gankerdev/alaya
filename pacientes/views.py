from django.shortcuts import render
from django.views.generic import ListView,CreateView,UpdateView,DeleteView
from django.core.urlresolvers import reverse_lazy
from .forms import *
from .models import *
from .urls import *
from django.views.generic import View
# Create your views here.

class PacienteList(ListView):
    model = Pacientes
    template_name = 'paciente_list.html'

class PacienteCreate(CreateView):
    model = Pacientes
    form_class = PacientesForm
    template_name = 'paciente_create.html'
    success_url = reverse_lazy('paciente:paciente_list')
    success_message = 'Paciente creado'

class PacienteUpdate(UpdateView):
    model = Pacientes
    form_class = PacientesForm
    template_name = 'paciente_update.html'
    success_url = reverse_lazy('paciente:paciente_list')


class PacienteDelete(DeleteView):
    model = Pacientes
    template_name = 'paciente_delete.html'
    success_url = reverse_lazy('paciente:paciente_listar')
