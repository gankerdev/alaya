from django.db import models

# Create your models here.
class FichaClinica(models.Model):
	enfermedades = models.CharField(max_length=255, null=False)
	tratamiento = models.CharField(max_length=255, null=False)
	fecha_enfermedad = models.DateTimeField(auto_now_add=False, auto_now=False)
	fecha_termino_tratamiento = models.DateTimeField(auto_now_add=False, auto_now=False)
	resultado_tratamiento = models.CharField(max_length=255, null=False)

def __str__(self):
	return self.enfermedades