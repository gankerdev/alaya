from django.contrib import admin
from .models import FichaClinica

# Register your models here.
class AdminFichaClinica(admin.ModelAdmin):
	list_display = ["enfermedades", "tratamiento", "fecha_enfermendad", "fecha_termino_tratamiento", "resultado_tratamiento"]
	list_filter = ["enfermedades"]
	list_search = ["enfermedades"]

admin.site.register(FichaClinica)