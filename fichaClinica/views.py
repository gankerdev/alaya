from django.shortcuts import render
from django.views.generic import ListView,CreateView,UpdateView,DeleteView
from django.core.urlresolvers import reverse_lazy
from .forms import *
from .models import *
from .urls import *
from django.views.generic import View
# Create your views here.

class FichaClinicaList(ListView):
    model = FichaClinica
    template_name = 'ficha_clinica_list.html'

class FichaClinicaCreate(CreateView):
    model = FichaClinica
    form_class = FichaClinicaForm
    template_name = 'ficha_clinica_create.html'
    success_url = reverse_lazy('fichaClinica:ficha_clinica_list')
    success_message = 'Ficha creada'

class FichaClinicaUpdate(UpdateView):
    model = FichaClinica
    form_class = FichaClinicaForm
    template_name = 'ficha_clinica_update.html'
    success_url = reverse_lazy('fichaClinica:ficha_clinica_list')


class FichaClinicaDelete(DeleteView):
    model = FichaClinica
    template_name = 'ficha_clinica_delete.html'
    success_url = reverse_lazy('fichaClinica:ficha_clinica_listar')
