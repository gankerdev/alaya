from django import forms

from .models import FichaClinica


class FichaClinicaForm(forms.ModelForm):
    class Meta:
        model = FichaClinica

        fields = [
            'enfermedades',
            'tratamiento',
            'fecha_enfermedad',
            'fecha_termino_tratamiento',
            'resultado_tratamiento',
        ]

        labels = {
            'enfermedades': 'Enfermedades',
            'tratamiento': 'Tratamiento',
            'fecha_enfermedad': 'Fecha inicio enfermedad',
            'fecha_termino_tratamiento': 'Fecha termino tratamiento',
            'resultado_tratamiento': 'resultado_tratamiento',
        }

        widgets = {
            'enfermedades': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Ingrese enfermedad...'}),
            'tratamiento': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Ingrese tratamiento...'}),
            'fecha_enfermedad': forms.SelectDateWidget(),
            'fecha_termino_tratamiento': forms.SelectDateWidget(),
            'resultado_tratamiento': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Ingrese resultado tratamiento...'}),
        }

