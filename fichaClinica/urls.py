from django.conf.urls import url, include
from django.contrib import admin
from .models import *
from .views import *
from .models import *

urlpatterns = [
    # ficha clinica
    url(r'^ficha_clinica/listar$', FichaClinicaList.as_view(), name='ficha_clinica_list'),
    url(r'^ficha_clinica/crear$', FichaClinicaCreate.as_view(), name='ficha_clinica_crear'),
    url(r'^ficha_clinica/editar$', FichaClinicaUpdate.as_view(), name='ficha_clinica_editar'),
    url(r'^ficha_clinica/eliminar$', FichaClinicaDelete.as_view(), name='ficha_clinica_eliminar'),
]